﻿using EnjoyDigitalUniversity.ViewModels;
using System.Linq;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace EnjoyDigitalUniversity.Controllers
{
    public class CourseListingController : RenderMvcController
    {
        #region Actions

        public override ActionResult Index(RenderModel model)
        {
            var courseListing = new CourseListingViewModel(CurrentPage);
            var departments = GetAllDepartmentIds();

            TryUpdateModel(courseListing);

            courseListing.Departments = departments;

            return CurrentTemplate(courseListing);
        }

        #endregion

        #region Helpers

        private System.Collections.Generic.IEnumerable<string> GetAllDepartmentIds()
        {
            var allTypes = Umbraco.DataTypeService.GetAllDataTypeDefinitions();
            var typeId = allTypes.First(x => "Department".InvariantEquals(x.Name)).Id;
            var departments = Umbraco.DataTypeService.GetPreValuesByDataTypeId(typeId);
            return departments;
        }

        #endregion
    }
}