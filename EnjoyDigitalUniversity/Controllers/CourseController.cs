﻿using System.Web.Mvc;
using EnjoyDigitalUniversity.Models;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using System.Web;
using EnjoyDigitalUniversity.Services;
using EnjoyDigitalUniversity.ViewModels;
using System.Linq;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using System.IO;

namespace EnjoyDigitalUniversity.Controllers
{
    public class CourseController : SurfaceController
    {
        public CourseController()
        {
        }

        public PartialViewResult Listing(string department)
        {
            var model = new CoursesViewModel();

            TryUpdateModel(model);

            var courses = CurrentPage.Children.Where(child => child.DocumentTypeAlias == "course");

            if (!string.IsNullOrEmpty(department))
            {
                model.Department = department;

                courses = courses.Where(child => child.GetPropertyValue<string>("department") == department);
            }

            model.Courses =
                courses
                    .OrderBy(child => child.GetPropertyValue<string>("title"))
                    .Select(course =>
                        new CourseViewModel
                        {
                            Title = course.GetPropertyValue<string>("title", false),
                            BodyText = course.GetPropertyValue<IHtmlString>("bodyText", false),
                            Department = course.GetPropertyValue<string>("department", false),
                            Url = course.Url
                        }
                );

            return PartialView(model);
        }

        public PartialViewResult Detail()
        {
            var model = Map(CurrentPage);

            return PartialView(model);
        }

        public PartialViewResult ApplyForm()
        {
            var student = new Student
            {
                CourseId = CurrentPage.Id
            };

            return PartialView(student);
        }

        [HttpPost]
        public ActionResult Apply(Student student)
        {
            if (ModelState.IsValid)
            {
                var studentService = new StudentService();

                if(studentService.Exists(student))
                {
                    studentService.Save(student);

                    TempData["successmessage"] = "Student updated";
                }
                else
                {
                    studentService.Save(student);

                    TempData["successmessage"] = "Student added";
                }
            }

            return CurrentUmbracoPage();
        }

        public CourseViewModel Map(IPublishedContent content)
        {
            var course = new CourseViewModel();

            course.Id = content.Id;
            course.Title = content.GetPropertyValue<string>("title");
            course.Department = content.GetPropertyValue<string>("department");
            course.BodyText = content.GetPropertyValue<IHtmlString>("bodyText");
            return course;
        }
    }
}