﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using EnjoyDigitalUniversity.Models;
using Newtonsoft.Json;

namespace EnjoyDigitalUniversity.Services
{
    public class StudentService : IStudentService
    {
        private const string REQUESTURLBASE = "http://app-technicaltestapi-dev.azurewebsites.net";

        public bool Exists(Student student)
            => GetStudent(student.EmailAddress) != null;

        public Student Save(Student student)
        {
            try
            {
                var studentID = GetStudentId(student.EmailAddress);

                if (studentID > 0) //Exists
                {
                    var existingStudentRequest = (HttpWebRequest)WebRequest.Create(REQUESTURLBASE + "/api/students/" + studentID);
                    existingStudentRequest.ContentType = "application/json";
                    existingStudentRequest.Method = "PUT";

                    student.Id = studentID;

                    var jsonExistingStudent = JsonConvert.SerializeObject(student);

                    using (var streamWriter = new StreamWriter(existingStudentRequest.GetRequestStream()))
                    {
                        streamWriter.Write(jsonExistingStudent);
                    }

                    var existingStudentResponse = (HttpWebResponse)existingStudentRequest.GetResponse();

                    if (existingStudentResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return student;
                    }
                }
                else
                {
                    var newStudentRequest = (HttpWebRequest)WebRequest.Create(REQUESTURLBASE + "/api/students");
                    newStudentRequest.ContentType = "application/json";
                    newStudentRequest.Method = "POST";

                    var jsonNewStudent = JsonConvert.SerializeObject(student);

                    using (var streamWriter = new StreamWriter(newStudentRequest.GetRequestStream()))
                    {
                        streamWriter.Write(jsonNewStudent);
                    }

                    var newStudentResponse = (HttpWebResponse)newStudentRequest.GetResponse();

                    if (newStudentResponse.StatusCode == HttpStatusCode.Created)
                    {
                        return student;
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO implement event log entry
            }

            return null;
        }

        public int GetStudentId(string emailAddress)
        {
            var student = GetStudent(emailAddress);

            return student != null ? student.Id : 0;
        }

        private Student GetStudent(string emailAddress)
        {
            try
            {
                var searchRequest = (HttpWebRequest)WebRequest.Create(REQUESTURLBASE + "/api/students/search");
                searchRequest.ContentType = "application/json";
                searchRequest.Method = "POST";

                var jsonEmailAddress =
                    JsonConvert.SerializeObject(new
                    {
                        EmailAddress = emailAddress
                    }
                );

                using (var streamWriter = new StreamWriter(searchRequest.GetRequestStream()))
                {
                    streamWriter.Write(jsonEmailAddress);
                }

                var searchResponse = (HttpWebResponse)searchRequest.GetResponse();

                if (searchResponse.StatusCode == HttpStatusCode.OK)
                {
                    using (var streamReader = new StreamReader(searchResponse.GetResponseStream()))
                    {
                        var searchResponseStream = streamReader.ReadToEnd();
                        if (!string.IsNullOrEmpty(searchResponseStream))
                        {
                            var result = JsonConvert.DeserializeObject<IEnumerable<Student>>(searchResponseStream);

                            return result.FirstOrDefault();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO implement event log entry
            }

            return null;
        }
    }
}