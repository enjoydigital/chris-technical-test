﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EnjoyDigitalUniversity.Models
{
    public class Student
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailAddress { get; set; }

        public int CourseId { get; set; }

        public Address Address { get; set; }

        public IEnumerable<Qualification> Qualifications { get; set; }
    }
}